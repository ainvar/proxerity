#pragma once
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>

#include <string>

//////////////////////////////////////////////////////////////////////////
//! Result of a file fetch
enum class FetchResult
    {
    Success,    //! Operation comepleted successfully
    Timeout,    //! A connection error occurred
    Error,      //! Timeout while fetching  
    };

//////////////////////////////////////////////////////////////////////////
//! File fetcher class. 
class WebFile
    {
    public:
        typedef std::shared_ptr<WebFile> Ptr;

    public:
        WebFile();
        WebFile(const std::string & url);
        ~WebFile();

    public:
        //! Fetches the file at the given url and places contents into contents_container
        //! \return Success in case of success, Timeout in case curl reported timeout, Error in any other case
        FetchResult fetch();

        //! Loads a file from disc. The given url is used as a file path. It's here just to make debug easier
        //! \return Success in case of success, Error in any other case
        FetchResult load();

        //! Returns a reference to current buffer
        const std::string & buffer() { return m_Buffer; };

    private:
        //! Writes received chars into buffer. It's called by curl
        size_t write_callback(char* ptr, size_t size, size_t nmemb);

        //! Called bu curl to print debug informations
        int debug_callback(curl_infotype, char *data, size_t size);

    private:
        //! URL of the file
        std::string m_Url;

        //! Fetched contents
        std::string m_Buffer;

    };

