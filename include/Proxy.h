#pragma once
#include "Logger.h"
#include "JsonSerializable.h"
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>

#include <map>
#include <list>
#include <vector>
#include <mutex>
#include <algorithm>
#include <chrono>
#include <sstream>

using namespace std::chrono;

//////////////////////////////////////////////////////////////////////////
class Domain
    {
    public:
        typedef std::shared_ptr<Domain> Ptr;

    public:
        Domain() {};
        ~Domain() {};

        //////////////////////////////////////////////////////////////////////////
        long long get_last_access_timespan_ms(system_clock::time_point & current_time) {
            return std::chrono::duration_cast<milliseconds>(current_time - m_LastAccess).count();
            };


    public:
        // last time we used this proxy to access the domain
        system_clock::time_point    m_LastAccess;
        // time to wait before accessing the domain
        int				            m_TimeToWait;
        // consecutive time we accessed the domain with this proxy
        int				            m_UseCount;
    };

typedef std::map<std::string, Domain::Ptr> Domains;

//////////////////////////////////////////////////////////////////////////
class ProxyManager;
class Proxy : public JsonSerializable
	{
	public:
		friend class ProxyManager;
		typedef std::shared_ptr<Proxy> Ptr;
		
	public:
		Proxy() : 
            m_ID(boost::lexical_cast<std::string>(g_uuid_generator())),
            m_BadProxy(false) 
            {};

		Proxy(std::string & ipaddress, int port, bool ssl_support) :
            m_ID(boost::lexical_cast<std::string>(g_uuid_generator())),
            m_IpAddress(ipaddress),
			m_Port(port),
			m_SSL(ssl_support)
			{};

		~Proxy() {}; 

	public:
		std::string address() { return m_IpAddress; };
        Proxy & address(const std::string & v) { m_IpAddress = v; return *this; };
		
        int port() { return m_Port; };
        Proxy & port(int v) { m_Port = v; return *this; };

        bool supports_ssl() { return m_SSL; };
        Proxy & supports_ssl(bool v) { m_SSL = v; return *this; };

        uint64_t use_count() { return m_UseCount; };
        Proxy & increment_use_count(uint64_t v = 1) { m_UseCount += v; return *this; };

        uint64_t fetched_pages_count() { return m_FetchedPageCount; };
        Proxy & increment_fetched_pages_count(uint64_t v = 1) { m_FetchedPageCount += v; return *this; };

		//////////////////////////////////////////////////////////////////////////
		bool add_domain(const std::string & domain) {
			bool ret(false);

			std::lock_guard<std::mutex> lock(m_Mutex);

			auto it = m_Domains.find(domain);
			if (it == m_Domains.end())
				{
				m_Domains[domain] = std::make_shared<Domain>();
				m_Domains[domain]->m_LastAccess = system_clock::now();
				m_Domains[domain]->m_UseCount++;
				ret = true;
				}

			return ret;
			};

		//////////////////////////////////////////////////////////////////////////
		Domain::Ptr get_domain(const std::string & domain) {
			Domain::Ptr ret(nullptr);

			std::lock_guard<std::mutex> lock(m_Mutex);

			auto it = m_Domains.find(domain);
			if (it != m_Domains.end())
				ret = it->second;

			return ret;
			};

		//////////////////////////////////////////////////////////////////////////
		bool has_domain(const std::string & domain) {
			std::lock_guard<std::mutex> lock(m_Mutex);
			return (m_Domains.find(domain) != m_Domains.end());
			};

		//////////////////////////////////////////////////////////////////////////
		long long get_domain_access_timespan_ms(const std::string & domain, system_clock::time_point & current_time) {
			long long ret(-1);
			std::lock_guard<std::mutex> lock(m_Mutex);

			auto it = m_Domains.find(domain);
			if (it != m_Domains.end())
				ret = it->second->get_last_access_timespan_ms(current_time);

			return ret;
			};

		//////////////////////////////////////////////////////////////////////////
		void clear() {
			std::lock_guard<std::mutex> lock(m_Mutex);
			m_Domains.clear();
			};

		//////////////////////////////////////////////////////////////////////////
		void mark_as_bad_proxy() {
			std::lock_guard<std::mutex> lock(m_Mutex);
			TRACE_NFO("Marking proxy %s:%d as a bad proxy", m_IpAddress.c_str(), m_Port);
			m_BadProxy = true;
			};

		//////////////////////////////////////////////////////////////////////////
		bool is_bad_proxy() {
			std::lock_guard<std::mutex> lock(m_Mutex);
			return m_BadProxy;
			};

        //////////////////////////////////////////////////////////////////////////
        std::string to_string() {
            std::stringstream ss;
            ss << m_IpAddress << ":" << m_Port << "[ssl:" << m_SSL << "]";
            return ss.str();
            }

        //////////////////////////////////////////////////////////////////////////
        SerializerError Serialize(SerializerContext & context, json::value & node) {
            JsonHelper::Serialize(context, node, "id", m_ID);
            JsonHelper::Serialize(context, node, "address", m_IpAddress);
            JsonHelper::Serialize(context, node, "port", m_Port);
            JsonHelper::Serialize(context, node, "ssl_support", m_SSL);
            return context.Error();
            };

	protected:
        static boost::uuids::random_generator g_uuid_generator;

        //! Identifier of the proxy
        std::string         m_ID;
		//! ip address of the proxy
		std::string		    m_IpAddress;
		//! port of the proxy
		int				    m_Port;
		//! does it support SSL?
		bool			    m_SSL;
		//! list of users
		Domains			    m_Domains;
		//! this tells it's a bad proxy
		bool			    m_BadProxy;
		//! mutex
		std::mutex		    m_Mutex;
        //! usage counter
        uint64_t            m_UseCount;
        //! fetched pages counter
        uint64_t            m_FetchedPageCount;
    };

typedef std::list<Proxy::Ptr> Proxies;



