#pragma once
#include "ProxyManager.h"

//////////////////////////////////////////////////////////////////////////
class ProxySpyParser : public ProxyListParser
    {
    public:
        ProxySpyParser() : ProxyListParser("proxyspy") {};
        virtual ~ProxySpyParser() {};

        //! Parses the given buffer and adds entries into the ProxyManager
        virtual ProxyListParser::Error parse(const std::string & file_buffer) override;
    };

