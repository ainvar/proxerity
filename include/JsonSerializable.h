#ifndef __JSON_SERIALIZABLE_H__
#define __JSON_SERIALIZABLE_H__

#include "EnumUtility.h"
#include "StringUtility.h"

#include <cpprest/json.h>
#include <string>
#include <memory>
#include <limits>
#include <type_traits>

using namespace web;

//////////////////////////////////////////////////////////////////////////
//! type traits to detect if a type is a shared pointer
template <class T> struct is_shared_ptr : std::false_type {};
template <class T> struct is_shared_ptr<std::shared_ptr<T> > : std::true_type {};

// This definitions is used in combination with std::enable_if
#define IS_SHARED_PTR(T) is_shared_ptr<T>::value

#define ENABLE_IF_INT(T)                typename std::enable_if<std::numeric_limits<T>::is_integer && !std::is_same<T, bool>::value, T>::type       
#define ENABLE_IF_BOOL(T)               typename std::enable_if<std::numeric_limits<T>::is_integer && std::is_same<T, bool>::value, T>::type       
#define ENABLE_IF_FLOAT(T)              typename std::enable_if<std::is_floating_point<T>::value, T>::type       
#define ENABLE_IF_STRING(T)             typename std::enable_if<std::is_same<T, std::string>::value, T>::type
#define ENABLE_IF_ENUM(T)               typename std::enable_if< std::is_enum<T>::value, T>::type
#define ENABLE_IF_JSON_SERIALIZABLE(T)  typename std::enable_if< std::is_base_of<JsonSerializable, T>::value, T>::type

//////////////////////////////////////////////////////////////////////////
//! Enumeration for serialization mode
enum class SerializerMode
	{
	//! The archiver is reading
	Load,
	//! The archiver is writing
	Save,
	};

//////////////////////////////////////////////////////////////////////////
//! Serialization errors
enum class SerializerError
	{
	Success,
	Fail,
	NoField,
	InvalidType,
	};

//////////////////////////////////////////////////////////////////////////
//! Base interface for serialization context
//! Setters uses Fluent Interface style
class SerializerContext
	{
	public:
		SerializerContext(SerializerMode mode) :
			m_Mode(mode),
			m_FailOnMissingField(false), 
			m_SkipEmptyValues(true),
            m_Error(SerializerError::Success)
			{}

		virtual ~SerializerContext() {}

	public:
		virtual SerializerMode Mode() { return m_Mode; };
		virtual SerializerContext & Mode(SerializerMode value) { m_Mode = value; return *this; };

		virtual bool FailOnMissingField() { return m_FailOnMissingField; };
		virtual SerializerContext &  FailOnMissingField(bool value) { m_FailOnMissingField = value; return *this; };

		virtual bool SkipEmptyValues() { return m_SkipEmptyValues; };
		virtual SerializerContext &  SkipEmptyValues(bool value) { m_SkipEmptyValues = value; return *this; };

        virtual SerializerError Error() { return m_Error; };
        virtual SerializerContext & Error(SerializerError value) { m_Error = value; return *this; };

	protected:
		//! Current archiver mode
		SerializerMode	m_Mode;
		//! Tells if load operation should fail in case a field is not present
		bool			m_FailOnMissingField;
		//! Tells if while saving the archiver should not write empty values ("").
		//! This could produce a more compact result (in size) but if when loading m_FailOnMissingField==true it will not load!
		bool			m_SkipEmptyValues;
        //! serialization error
        SerializerError m_Error;
	};

//////////////////////////////////////////////////////////////////////////
//! Base interface for all serializable objects
class JsonSerializable
	{
	public:
		JsonSerializable() {};

    public:
        //////////////////////////////////////////////////////////////////////////
        json::value to_json() {
            json::value vv = json::value::object();
            SerializerContext ctx(SerializerMode::Save);
            Serialize(ctx, vv);
            return vv;
            };

        //////////////////////////////////////////////////////////////////////////
        operator json::value() { return to_json(); }

        //////////////////////////////////////////////////////////////////////////
        SerializerError from_json(json::value & vv) {
            SerializerContext ctx(SerializerMode::Load);
            Serialize(ctx, vv);
            return ctx.Error();
            };

    public:
        virtual SerializerError Serialize(SerializerContext & context, json::value & node) = 0;

    };

struct JsonHelper
    {
    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, int8_t & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, int8_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, uint8_t & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, uint8_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, int16_t & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, int16_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, uint16_t & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_boolean(); }, [](json::value & n) { return n.as_bool(); }, [](json::value & n, uint16_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, int32_t & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, int32_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, uint32_t & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_boolean(); }, [](json::value & n) { return n.as_bool(); }, [](json::value & n, uint32_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, int64_t & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, int64_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, uint64_t & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_boolean(); }, [](json::value & n) { return n.as_bool(); }, [](json::value & n, uint64_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, bool & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_boolean(); }, [](json::value & n) { return n.as_bool(); }, [](json::value & n, bool v) { n = json::value::boolean(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, double & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_double(); }, [](json::value & n) { return n.as_double(); }, [](json::value & n, double v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, float & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_double(); }, [](json::value & n) { return (float)n.as_double(); }, [](json::value & n, float v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, std::string & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_string(); }, [](json::value & n) { return utility::conversions::to_utf8string(n.as_string()); }, [&](json::value & n, std::string & v) { n = json::value::string(utility::conversions::to_string_t(v)); });
        }

    //////////////////////////////////////////////////////////////////////////
    template<typename T>
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, ENABLE_IF_ENUM(T) & value) {
        return Serialize(context, node, name, value, [](json::value & n) { return n.is_string(); }, [](json::value & n) { return EnumMapper::GetValue(n.as_string(), (T)0); }, [](json::value & n, T & v) { std::string vstr = EnumMapper::GetText(v); n = json::value::string(vstr); });
        }

    //////////////////////////////////////////////////////////////////////////
    template<typename T>
    static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, std::shared_ptr<ENABLE_IF_JSON_SERIALIZABLE(T)> & value) {
        SerializerError ret(SerializerError::Fail);

        if (context.Mode() == SerializerMode::Load)
            {
            if (node.has_field(utility::conversions::to_string_t(name)))
                {
                value = std::make_shared<T>();
                ret = value->Serialize(context, node[name]);
                }
            }
        else
            {
            json::value new_node = json::object();
            ret = value->Serialize(context, new_node);

            if (ret == SerializerError::Success)
                node[utility::conversions::to_string_t(name)] = new_node;
            }
        return ret;
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, int8_t & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, int8_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, uint8_t & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, uint8_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, int16_t & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, int16_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, uint16_t & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, uint16_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, int32_t & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, int32_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, uint32_t & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, uint32_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, int64_t & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, int64_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, uint64_t & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_integer(); }, [](json::value & n) { return n.as_integer(); }, [](json::value & n, uint64_t v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, bool & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_boolean(); }, [](json::value & n) { return n.as_bool(); }, [](json::value & n, bool v) { n = json::value::boolean(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, float & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_double(); }, [](json::value & n) { return (float)n.as_double(); }, [](json::value & n, float v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, double & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_double(); }, [](json::value & n) { return n.as_double(); }, [](json::value & n, double v) { n = json::value::number(v); });
        }

    //////////////////////////////////////////////////////////////////////////
    static SerializerError Serialize(SerializerContext & context, json::value & node, std::string & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_string(); }, [](json::value & n) { return utility::conversions::to_utf8string(n.as_string()); }, [](json::value & n, std::string & v) { n = json::value::string(utility::conversions::to_string_t(v)); });
        }

    //////////////////////////////////////////////////////////////////////////
    template<typename T>
    static SerializerError Serialize(SerializerContext & context, json::value & node, ENABLE_IF_ENUM(T) & value) {
        return Serialize(context, node, value, [](json::value & n) { return n.is_string(); }, [](json::value & n) { return EnumMapper::GetValue(n.as_string(), (T)0); }, [](json::value & n, T & v) { std::string vstr = EnumMapper::GetText(v); n = json::value::string(vstr); });
        }

    //////////////////////////////////////////////////////////////////////////
    template<typename C>
    static SerializerError SerializeArray(SerializerContext & context, json::value & node, std::string name, C & container) {
        SerializerError ret(SerializerError::Success);

        if (context.Mode() == SerializerMode::Load)
            {
            if (node.has_field(utility::conversions::to_string_t(name)))
                {
                json::value & vv = node[utility::conversions::to_string_t(name)];
                if (vv.is_array())
                    {
                    // Note the "as_object()" method calls
                    json::array aa = vv.as_array();
                    for (auto jv : aa)
                        {
                        typename C::value_type v;
                        SerializerError item_err = archive_array_item<typename C::value_type>(context, jv, v);
                        if (item_err == SerializerError::Success)
                            container.push_back(v);
                        else
                            {
                            ret = item_err;
                            break;
                            }
                        }
                    }
                }
            else if (context.FailOnMissingField())
                ret = SerializerError::NoField;
            }
        else
            {
            json::value array_node = json::value::array();
            int cnt = 0;
            for (auto & item : container)
                {
                json::value item_node = json::value::object();
                SerializerError item_err = archive_array_item<typename C::value_type>(context, item_node, item);
                if (item_err == SerializerError::Success)
                    array_node[cnt++] = item_node;
                else
                    {
                    ret = item_err;
                    break;
                    }
                }

            node[utility::conversions::to_string_t(name)] = array_node;
            }

        return ret;
        }

    //////////////////////////////////////////////////////////////////////////
    template<typename T>
    static SerializerError Deserialize(std::string & source, T & object)
        {
        return Deserialize<T>(source.data(), source.size(), object);
        }

    //////////////////////////////////////////////////////////////////////////
    template<typename T>
    static SerializerError Deserialize(const char * data, uint32_t size, T & object)
        {
        TRACE_CALL(__FUNCTION__);

        SerializerError ret(SerializerError::Success);

        json::value vv = json::object();
        SerializerContext context(SerializerMode::Load);
        context.FailOnMissingField(false);

        json::value vv = json::value::parse(data, size);
        if (vv.is_object())
            ret = object.Serialize(context, vv);
        else
            ret = SerializerError::Fail;

        return ret;
        }

    //////////////////////////////////////////////////////////////////////////
    template<typename T>
    static SerializerError Serialize(T & object, std::string & destination)
        {
        TRACE_CALL(__FUNCTION__);

        SerializerError ret(SerializerError::Success);

        json::value vv = json::object();
        SerializerContext context(SerializerMode::Save);
        context.SkipEmptyValues(true);

        ret = object.Serialize(context, vv);

        // copy!!!!
        if (ret == SerializerError::Success)
            destination = vv.to_string();

        return ret;
        }

    private:
        //////////////////////////////////////////////////////////////////////////
        // Serializes a child, by name, of the given node
        //! CheckPred = bool(json::value &) - used to check if a field is valid and it's type is same as wanted type
        //! GetPred = T (json::value &) - Gets the value
        //! SetPred = void(json::value &, T & value) - Sets the value
        template<typename T, typename CheckPred, typename GetPred, typename SetPred>
        static SerializerError Serialize(SerializerContext & context, json::value & node, const std::string & name, T & value, CheckPred checkFunc, GetPred getFunc, SetPred setFunc)
            {
            SerializerError ret(context.Error());
            if (ret == SerializerError::Success)
                if (context.Mode() == SerializerMode::Load)
                    {
                    utility::string_t nname = utility::conversions::to_string_t(name);
                    if (node.has_field(nname))
                        ret = Serialize(context, node[nname], value, checkFunc, getFunc, setFunc);
                    else if (context.FailOnMissingField())
                        ret = SerializerError::NoField;
                    }
                else
                    {
                    json::value vv = json::value();
                    ret = Serialize(context, vv, value, checkFunc, getFunc, setFunc);
                    node[utility::conversions::to_string_t(name)] = vv;
                    }

            return ret;
            }

        //////////////////////////////////////////////////////////////////////////
        // Serializes the given node
        //! CheckPred = bool(json::value &) - used to check if a field is valid and it's type is same as wanted type
        //! GetPred = T (json::value &) - Gets the value
        //! SetPred = void(json::value &, T & value) - Sets the value
        template<typename T, typename CheckPred, typename GetPred, typename SetPred>
        static SerializerError Serialize(SerializerContext & context, json::value & node, T & value, CheckPred checkFunc, GetPred getFunc, SetPred setFunc)
            {
            SerializerError ret(context.Error());
            if (ret == SerializerError::Success)
                {
                if (context.Mode() == SerializerMode::Load)
                    {
                    if (checkFunc(node))
                        {
                        value = getFunc(node);
                        ret = SerializerError::Success;
                        }
                    else
                        ret = SerializerError::InvalidType;
                    }
                else
                    {
                    setFunc(node, value);
                    ret = SerializerError::Success;
                    }
                }

            return ret;
            }

        //! helper struct used to archive a container item if derived from JsonSerializable
        template<typename Q, bool is_serializable_object>
        struct array_item_serializer {
            template<typename T = Q>
            static SerializerError archive_array_item(SerializerContext & context, json::value & node, typename std::enable_if<!std::is_pointer<T>::value && !IS_SHARED_PTR(T), T>::type & item) {
                return item.Serialize(context, node);
                }

            template<typename T = Q>
            static SerializerError archive_array_item(SerializerContext & context, json::value & node, typename std::enable_if<std::is_pointer<T>::value && !IS_SHARED_PTR(T), T>::type & item) {
                if (context.Mode() == SerializerMode::Load)
                    item = new typename std::remove_pointer<Q>::type();
                return item->Serialize(context, node);
                }

            //#ifdef JSON_SERIALIZER_SHARED_PTR_SUPPORT
            template<typename T = Q>
            static SerializerError archive_array_item(SerializerContext & context, json::value & node, typename std::enable_if<!std::is_pointer<T>::value && IS_SHARED_PTR(T), T>::type & item) {
                if (context.Mode() == SerializerMode::Load)
                    item = std::make_shared<typename T::element_type>();
                return item->Serialize(context, node);
                }
            //#endif
            };

        //! helper struct used to archive a container item. Partial specialization to handle items NOT derived from JsonSerializable
        template<typename Q>
        struct array_item_serializer<Q, false> {
            template<typename T = Q>
            static SerializerError archive_array_item(SerializerContext & context, json::value & node, T & item) {
                return Serialize(context, node, item);
                }

            template<typename T = Q>
            static SerializerError archive_array_item(SerializerContext & context, json::value & node, T* & item) {
                item = new T();
                return Serialize<typename std::remove_pointer<T>::type>(context, node, *item);
                }

            //#ifdef JSON_SERIALIZER_SHARED_PTR_SUPPORT
            template<typename T = Q>
            static SerializerError archive_array_item(SerializerContext & context, json::value & node, std::shared_ptr<T> & item) {
                item = std::make_shared<T>();
                return Serialize(context, node, *item.get());
                }
            //#endif
            };


        //#ifdef JSON_SERIALIZER_SHARED_PTR_SUPPORT
        //! helper for array items
        template <typename T>
        static SerializerError archive_array_item(SerializerContext & context, json::value & node, typename std::enable_if< is_shared_ptr<T>::value, T>::type & item) {
            return array_item_serializer<T, std::is_base_of<JsonSerializable, typename T::element_type>::value>::archive_array_item(context, node, item);
            }

        //! helper for array items
        template <typename T>
        static SerializerError archive_array_item(SerializerContext & context, json::value & node, typename std::enable_if< !is_shared_ptr<T>::value && std::is_pointer<T>::value, T>::type & item) {
            return array_item_serializer<T, std::is_base_of<JsonSerializable, typename std::remove_pointer<T>::type >::value>::archive_array_item(context, node, item);
            }

        //! helper for array items
        template <typename T>
        static SerializerError archive_array_item(SerializerContext & context, json::value & node, typename std::enable_if< !is_shared_ptr<T>::value && !std::is_pointer<T>::value, T>::type & item) {
            return array_item_serializer<T, std::is_base_of<JsonSerializable, T>::value>::archive_array_item(context, node, item);
            }

        //#else
        // 		//! helper for array items
        // 		template <typename T>
        // 		static JsonSerializationError archive_array_item(JsonSerializationContext & context, json::value & node, T & item) {
        // 			return array_item_serializer<T, std::is_base_of<JsonSerializable, typename std::remove_pointer<T>::type >::value>::archive_array_item(context, node, item);
        // 			}
        //#endif
    };

#endif //__JSON_SERIALIZABLE_H__