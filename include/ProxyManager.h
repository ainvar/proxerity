#pragma once

#include "Singleton.h"
#include "Proxy.h"
#include "TimerThread.h"

#include <soci/soci.h>
#include <soci/mysql/soci-mysql.h>

#include <chrono>

using namespace std::chrono;
using namespace soci;

//////////////////////////////////////////////////////////////////////////
//! Interface for proxy list parsers
class ProxyListParser
    {
    public:
        typedef std::shared_ptr<ProxyListParser> Ptr;

        ProxyListParser(const std::string & name) : m_Name(name) {};
        virtual ~ProxyListParser() {};

        //////////////////////////////////////////////////////////////////////////
        enum class Error
            {
            Success,
            GenericFailure,
            FormatError,
            };

        //! returns the name of the parser
        const std::string & name() { return m_Name; };
        //! Parses the given buffer and adds entries into the ProxyManager
        virtual ProxyListParser::Error parse(const std::string & file_buffer) = 0;
    
    protected:
        std::string     m_Name;
    };

typedef std::list<ProxyListParser::Ptr> ProxyListParsers;


//////////////////////////////////////////////////////////////////////////
class ProxyListFetcher
    {
    public:
        typedef std::shared_ptr<ProxyListFetcher> Ptr;

        //////////////////////////////////////////////////////////////////////////
        enum class Error
            {
            Success,
            GenericFailure,
            NoConfig,
            };

    public:
        ProxyListFetcher() {};
        ProxyListFetcher(ProxyListParser::Ptr parser) : m_Parser(parser) {};

        const std::string & name() { return m_Parser->name(); };

        Error start();
        Error stop();

    protected:
        ProxyListParser::Ptr    m_Parser;
        TimerThread::Ptr        m_ListUpdater;
    };

typedef std::list<ProxyListFetcher::Ptr> ProxyListFetchers;


//////////////////////////////////////////////////////////////////////////
//! Manages proxy entities and persistence
class ProxyManager : public Singleton<ProxyManager>
	{
	public:
		ProxyManager();
		~ProxyManager();

        //////////////////////////////////////////////////////////////////////////
        enum class Error
            {
            Success,
            GenericFailure,
            Timeout,
            ConnectionError,
            FormatError,
            AlreadyRegistered,
            NotFound,
            DbError,
            };

	public:
        //! starts the manager
		Error start();
        //! stops the manager
		Error stop();
        //! Registers a proxy list parser
        Error register_proxy_list_parser(ProxyListParser::Ptr parser);
        //! Unregisters a proxy list parser
        Error register_proxy_list_parser(const std::string & name);
        //! gets the best proxy to use for accessing the given domain
        Error add_proxy(Proxy::Ptr proxy);
        //! gets the best proxy to use for accessing the given domain
		Proxy::Ptr get_proxy(const std::string & domain);

	private:
        //! Loads proxies from database
        Error load_from_db();
        //! Returns a proxy list parser by its name
        ProxyListFetcher::Ptr get_proxy_list_parser(const std::string & name);
		//! gets a proxy that has never accessed the given domain
		Proxy::Ptr get_unused_proxy(const std::string & domain_name);
		//! gets the proxy that has the oldest access to given domain
		Proxy::Ptr get_oldest_used_proxy(const std::string & domain_name, system_clock::time_point & current_time);

	protected:
		//! thread used to obtain an updated list of proxies
		TimerThread::Ptr	m_ProxyDownloader;
		//! loaded proxies
		Proxies				m_Proxies;
		//! data mutex
		std::mutex			m_Mutex;
		//! Tells if we completed loading proxies
		bool				m_LoadingComplete;
        //! List of registered proxy list parsers
        ProxyListFetchers   m_ProxyListFetchers;
        //! db connection string
        std::string         m_ConnectionString;
        //! current soci backend factory
        const backend_factory &   m_SociBackEnd;

	};

