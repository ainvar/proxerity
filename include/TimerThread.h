#pragma once

#include <thread>
#include <chrono>
#include <atomic>
#include <functional>
#include <memory>

using namespace std::chrono;

//////////////////////////////////////////////////////////////////////////
// Timer callback type
typedef std::function<int(void *)> TimerCallback;


//////////////////////////////////////////////////////////////////////////
class TimerThread
	{
	public:
		typedef std::shared_ptr<TimerThread> Ptr;

	public:
		TimerThread(milliseconds time, TimerCallback callback, bool one_shot, void * argument = nullptr);
		~TimerThread();

		bool start();
		bool stop();
		bool running() { return m_Running; };

	protected:
        milliseconds elapsed() { return duration_cast<milliseconds>(high_resolution_clock::now() - m_StartTime); };
		void reset() { m_StartTime = high_resolution_clock::now(); };

	private:
        //! Tells the thread if it must keep running
		std::atomic<bool>	                m_Run;
        //! Tells caller if the thread is running
		bool				                m_Running;
        //! If true, the timed callback is called only once and then the thread dies
		bool				                m_OneShot;
        //! Period of the timer in milliseconds
        milliseconds	                    m_Period;
        //! Worker thread
		std::thread			                m_Thread;
        //! Argument to pass to callback
		void *				                m_Argument;
        //! Callback to call after period elapsed
		TimerCallback		                m_Callback;
        //! Start time of the thread
        high_resolution_clock::time_point	m_StartTime;
		
	};

