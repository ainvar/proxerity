#pragma once
#include "../include/RestController.h"
#include "../include/JsonSerializable.h"
#include <string>

//////////////////////////////////////////////////////////////////////////
struct ProxyGetArguments : public JsonSerializable
    {
    SerializerError Serialize(SerializerContext & context, json::value & node) { 
        JsonHelper::Serialize(context, node, "domain", m_Domain);
        return context.Error();
        };

    std::string     m_Domain;
    };

//////////////////////////////////////////////////////////////////////////
struct ProxyPostArguments : public JsonSerializable
    {
    SerializerError Serialize(SerializerContext & context, json::value & node) {
        JsonHelper::Serialize(context, node, "id", m_ID);
        JsonHelper::Serialize(context, node, "is_bad_proxy", m_IsBadProxy);
        JsonHelper::Serialize(context, node, "fetched_pages", m_FetchedPageCount);
        return context.Error();
        };

    std::string     m_ID;
    bool            m_IsBadProxy;
    uint64_t        m_FetchedPageCount;
    };

//////////////////////////////////////////////////////////////////////////
class ProxerityController : public RestController
    {
    public:
        ProxerityController(const std::string & url);
        ~ProxerityController();

    protected:
        virtual void on_get(http_request message);
        virtual void on_post(http_request message);
    };

