#pragma once

#include "Singleton.h"
#include "JsonSerializable.h"

#include <list>
#include <memory>
#include <mutex>

//////////////////////////////////////////////////////////////////////////
//! Configuration entry for a proxy list
class ProxyListConfig : public JsonSerializable
	{
    public:
        typedef std::shared_ptr<ProxyListConfig> Ptr;

    public:
        //! Serializes/Deserializes json objects
        virtual SerializerError Serialize(SerializerContext & context, json::value & node) {
            JsonHelper::Serialize(context, node, "name", m_Name);
            JsonHelper::Serialize(context, node, "url", m_URL);
            JsonHelper::Serialize(context, node, "parser", m_Parser);
            JsonHelper::Serialize(context, node, "update_time", m_UpdateTime);
            return context.Error();
            };

    public:
        //! Name of the configuration
        std::string     m_Name;
        //! Url of the file to fetch
	    std::string		m_URL;
        //! Name of the parser to use to parse the list
        std::string     m_Parser;
        //! Period for url fetches (in minutes)
	    int				m_UpdateTime;
	};

typedef std::list<ProxyListConfig::Ptr> ProxyListConfigs;

//////////////////////////////////////////////////////////////////////////
//! Manages configuration file
class ConfigurationManager : public Singleton<ConfigurationManager>
	{
	public:
		ConfigurationManager();
		~ConfigurationManager();

        //////////////////////////////////////////////////////////////////////////
        enum class Error
            {
            Success,
            GenericFailure,
            Empty,
            FileNotFound,
            AccessDenied,
            FormatError,
            ParseErorr,
            };

	public:
        //! Loads the configuration from the given file
		Error load(const std::string & filename);

        //! Returns the configuration for the given parser
        ProxyListConfig::Ptr get_config(const std::string & parser);

	protected:
        //! List of loaded configurations
        ProxyListConfigs    m_ProxyListConfigs;
        //! Synch mutex
        std::mutex          m_Mutex;


	};

