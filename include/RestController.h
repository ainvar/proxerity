#pragma once

#include <string>

#include <cpprest/http_listener.h>

using namespace web;
using namespace web::http;
using namespace web::http::experimental::listener;

// Controller method support mask
#define SUPPORT_GET     0x0001
#define SUPPORT_POST    0x0002
#define SUPPORT_PUT     0x0004
#define SUPPORT_DEL     0x0008

//////////////////////////////////////////////////////////////////////////
class RestController
    {
    public:
        RestController(const std::string & url, unsigned support_mask);
        ~RestController();

    public:
        virtual void start();
        virtual void stop();

    protected:
        virtual void on_get(http_request message) { message.reply(http::status_codes::InternalError); };
        virtual void on_post(http_request message) { message.reply(http::status_codes::InternalError); };
        virtual void on_put(http_request message) { message.reply(http::status_codes::InternalError); };
        virtual void on_del(http_request message) { message.reply(http::status_codes::InternalError); };

    protected:
        void ApplyStartMask();

    protected:
        http_listener   m_Listener;
        unsigned        m_SupportMask;

    };

