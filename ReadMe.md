# *proxerity* 
##
  Proxy prosperity for web-crawlers
<br>
<br>

### Overview
##
Often web-crawlers have to use proxy servers to change IP address and avoid being banned by targeted site servers.

This tool manages periodically downloads lists of many proxy servers and keeps tracks of damins visited using each proxy in order to avoid to use the same one for multiple connections to the same targeted machines.

The result is that when you can call proxerity REST service, you get a JSON object representing a proxy you can freely use to connect to domain.
  
Each proxy has a rank and you can contribute to ranking by calling a REST service and telling if the given proxy failed. 
<br>
<br>
### Thechy stuff
## 
The tool is written in C++ and uses Microsoft C++ REST SDK to offer RESTful services.
MySQL database is used to store proxy data, stats and ranking
To fetch files, curl and curlpp have been used. 
<br>
<br>
### Platforms
## 
Actually only VS2015 solution exists but the code is organized to allow to add other project files without making to much mess.
The code should by quite portable because nothing more than cross-platform libs and std C++11 code have been used.
