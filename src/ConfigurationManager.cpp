#include "../include/ConfigurationManager.h"
#include "../include/Logger.h"

#include <fstream>

//////////////////////////////////////////////////////////////////////////
ConfigurationManager::ConfigurationManager()
	{
	}

//////////////////////////////////////////////////////////////////////////
ConfigurationManager::~ConfigurationManager()
	{
	}

//////////////////////////////////////////////////////////////////////////
ConfigurationManager::Error ConfigurationManager::load(const std::string & filename)
	{
	Error ret(Error::FileNotFound);

	TRACE_DBG("%s - Loading : %s", __FUNCTION__, filename.c_str());

    std::lock_guard<std::mutex> locker(m_Mutex);

	std::ifstream f(filename);
	if (f.is_open())
		{
        // load all file
        json::value v = json::value::parse(f);
        SerializerContext ctx(SerializerMode::Load);
        m_ProxyListConfigs.clear();
        SerializerError err = JsonHelper::SerializeArray(ctx, v, "proxy_list_fetchers", m_ProxyListConfigs);
        if (err != SerializerError::Success)
            {
            TRACE_DBG("%s - Error while parsing : %d", __FUNCTION__, err);
            ret = Error::ParseErorr;
            }
        else
            ret = Error::Success;
		}

	return ret;
	}

//////////////////////////////////////////////////////////////////////////
ProxyListConfig::Ptr ConfigurationManager::get_config(const std::string & parser)
    {
    ProxyListConfig::Ptr ret(nullptr);

    std::lock_guard<std::mutex> locker(m_Mutex);

    auto it = std::find_if(m_ProxyListConfigs.begin(), m_ProxyListConfigs.end(), [&](ProxyListConfig::Ptr cfg) {return (cfg->m_Parser == parser); });
    if (it != m_ProxyListConfigs.end())
        ret = *it;

    return ret;
    }
