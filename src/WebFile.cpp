#include "../include/WebFile.h"
#include "../include/Logger.h"
#include "../include/StringUtility.h"

#include <fstream>

//////////////////////////////////////////////////////////////////////////
WebFile::WebFile()
    {
    }

//////////////////////////////////////////////////////////////////////////
WebFile::WebFile(const std::string & url) :  m_Url(url)
    {
    }

//////////////////////////////////////////////////////////////////////////
WebFile::~WebFile()
    {
    }

//////////////////////////////////////////////////////////////////////////
FetchResult WebFile::fetch()
    {
    FetchResult ret(FetchResult::Error);

    TRACE_NFO("%s - Fetching %s...", __FUNCTION__, m_Url.c_str());

    try
        {
        // That's all that is needed to do cleanup of used resources (RAII style).
        curlpp::Cleanup myCleanup;
        curlpp::Easy request;

        using namespace curlpp::Options;
        request.setOpt(Verbose(true));

        // Set the writer callback to enable cURL 
        // to write result in a memory area
        request.setOpt(WriteFunction(curlpp::types::WriteFunctionFunctor(this, &WebFile::write_callback)));
        request.setOpt(DebugFunction(curlpp::types::DebugFunctionFunctor(this, &WebFile::debug_callback)));
        request.setOpt(FollowLocation(true)); // add this one, it seems to spawn redirect 301 header
        request.setOpt(UserAgent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13")); // spoof
        request.setOpt(Url(m_Url));
        request.perform();

        long http_code = 0;
        curl_easy_getinfo(request.getHandle(), CURLINFO_RESPONSE_CODE, &http_code);

        if (http_code != 200)
            ret = FetchResult::Error;
        else
            ret = FetchResult::Success;
        }
    catch (curlpp::RuntimeError & e)
        {
        TRACE_ERR("%s - Exception caught : %s ", __FUNCTION__, e.what());
        ret = FetchResult::Error;
        }
    catch (curlpp::LogicError & e)
        {
        TRACE_ERR("%s - Exception caught : %s ", __FUNCTION__, e.what());
        ret = FetchResult::Error;
        }

    return ret;
    }

//////////////////////////////////////////////////////////////////////////
FetchResult WebFile::load()
    {
    FetchResult ret(FetchResult::Error);

    std::ifstream istr(m_Url);
    if (istr.is_open())
        {
        m_Buffer.append(istreambuf_iterator<char>(istr), istreambuf_iterator<char>());
        istr.close();
        ret = FetchResult::Success;
        }

    return ret;
    }

//////////////////////////////////////////////////////////////////////////
std::size_t WebFile::write_callback(char * ptr, std::size_t size, std::size_t nmemb)
    {
    // Calculate the real size of the incoming buffer
    size_t realsize = size * nmemb;

    m_Buffer.append(ptr, realsize);

    // return the real size of the buffer...
    return realsize;
    };

//////////////////////////////////////////////////////////////////////////
int WebFile::debug_callback(curl_infotype infotype, char * data, std::size_t size)
    {
    int ret(size);
    std::string text(data, size);
    trim(text);

    switch (infotype)
        {
        case CURLINFO_TEXT:
        case CURLINFO_HEADER_IN:
        case CURLINFO_HEADER_OUT:
        case CURLINFO_SSL_DATA_IN:
        case CURLINFO_SSL_DATA_OUT:
            TRACE_DBG("CURL: %s", text.c_str());
            break;

        case CURLINFO_DATA_IN:
        case CURLINFO_DATA_OUT:
        default:
            break;
        }

    return ret;
    }
