#include "../include/ProxySpyParser.h"
#include "../include/StringUtility.h"
#include "../include/Proxy.h"
#include "../include/ProxyManager.h"

//////////////////////////////////////////////////////////////////////////
ProxyListParser::Error ProxySpyParser::parse(const std::string & file_buffer)
    {
    ProxyListParser::Error ret(ProxyListParser::Error::Success);

    TRACE_DBG("%s is parsing buffer", name().c_str());

    std::vector<std::string> lines = ::split(file_buffer, '\n');
    high_resolution_clock::time_point process_start = high_resolution_clock::now();

    int first_line = 2;
    int cnt = 0;
    for (auto line : lines)
        {
        if ((cnt >= first_line) && (!line.empty()) && (is_numeric(line.substr(0, 3))))
            {
            std::vector<std::string> strs = split(line, ' ');
            if ((strs.size() >= 2) && (strs.size() < 5))
                {
                strs = split(strs[0], ':');
                Proxy::Ptr proxy = std::make_shared<Proxy>();
                proxy->address(strs[0]).port(::atoi(strs[1].c_str())).supports_ssl(false);
                 
                ProxyManager::Error perr = ProxyManager::Instance().add_proxy(proxy);
                if (perr != ProxyManager::Error::Success)
                    ret = ProxyListParser::Error::GenericFailure;
                }
            else
                TRACE_DBG("Wrong line format : %s", line.c_str());
            }

        cnt++;
        }

    milliseconds process_duration = duration_cast<milliseconds>(high_resolution_clock::now() - process_start);
    TRACE_DBG("Proxy list processed %d entried in : %ldms", cnt, process_duration.count());

    return ret;
    }

