#include "../include/RestController.h"
#include "../include/Logger.h"


//////////////////////////////////////////////////////////////////////////
RestController::RestController(const std::string & url, unsigned support_mask) :
    m_Listener(utility::conversions::to_string_t(url)),
    m_SupportMask(support_mask)
    {
    }

//////////////////////////////////////////////////////////////////////////
RestController::~RestController()
    {
    }

//////////////////////////////////////////////////////////////////////////
void RestController::start()
    {
    ApplyStartMask();

    try
        {
        m_Listener.open().then([&]() { TRACE_NFO("REST Controller started"); }).wait();
        }
    catch (exception const & e)
        {
        cout << e.what() << endl;
        }
    }

//////////////////////////////////////////////////////////////////////////
void RestController::stop()
    {
    m_Listener.close();
    }

//////////////////////////////////////////////////////////////////////////
void RestController::ApplyStartMask()
    {
    if (m_SupportMask & SUPPORT_GET)
        {
        m_Listener.support(methods::GET, [=](http_request message) {
            std::cout << "Serving GET" << std::endl;
            try {
                this->on_get(message);
                }
            catch (...) {
                message.reply(http::status_codes::InternalError);
                }
            });
        }

    if (m_SupportMask & SUPPORT_POST)
        {
        m_Listener.support(methods::POST, [=](http_request message) {
            std::cout << "Serving POST" << std::endl;
            try {
                this->on_post(message);
                }
            catch (...) {
                message.reply(http::status_codes::InternalError);
                }
            });
        }

    if (m_SupportMask & SUPPORT_PUT)
        {
        m_Listener.support(methods::PUT, [=](http_request message) {
            std::cout << "Serving PUT" << std::endl;
            try {
                this->on_put(message);
                }
            catch (...) {
                message.reply(http::status_codes::InternalError);
                }
            });
        }

    if (m_SupportMask & SUPPORT_DEL)
        {
        m_Listener.support(methods::DEL, [=](http_request message) {
            std::cout << "Serving DEL" << std::endl;
            try {
                this->on_del(message);
                }
            catch (...) {
                message.reply(http::status_codes::InternalError);
                }
            });
        }
    }
