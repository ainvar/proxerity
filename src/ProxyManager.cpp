#include "../include/ProxyManager.h"
#include "../include/Logger.h"
#include "../include/StringUtility.h"
#include "../include/WebFile.h"
#include "../include/StringUtility.h"
#include "../include/ConfigurationManager.h"

#include <fstream>
#include <chrono>
#include <algorithm>
#include <memory>

//////////////////////////////////////////////////////////////////////////
ProxyListFetcher::Error ProxyListFetcher::start()
    {
    ProxyListFetcher::Error ret(ProxyListFetcher::Error::GenericFailure);
    TRACE_NFO("Starting fetcher for proxy list %s", name().c_str());

    ProxyListConfig::Ptr config = ConfigurationManager::Instance().get_config(name());
    if (config)
        {
        milliseconds period(config->m_UpdateTime * 60 * 1000);
        TimerCallback callback = [=](void *) {
            // get the configuration
            ProxyListConfig::Ptr config = ConfigurationManager::Instance().get_config(name());
            if (config)
                {
                WebFile web_file(config->m_URL);
                FetchResult fres = web_file.fetch();
                if (fres == FetchResult::Success) {
                    m_Parser->parse(web_file.buffer());
                    }
                }
            return 0;
            };

        m_ListUpdater = std::make_shared<TimerThread>(period, callback, false, nullptr);
        if (m_ListUpdater->start())
            ret = ProxyListFetcher::Error::Success;
        }
    else
        {
        TRACE_NFO("Cannot find configuration forr proxy list %s", name().c_str());
        ret = ProxyListFetcher::Error::NoConfig;
        }
    
    return ret;
    }

//////////////////////////////////////////////////////////////////////////
ProxyListFetcher::Error ProxyListFetcher::stop()
    {
    ProxyListFetcher::Error ret(ProxyListFetcher::Error::GenericFailure);
    TRACE_NFO("Stopping fetcher for proxy list %s", name().c_str());

    if (m_ListUpdater->stop())
        ret = ProxyListFetcher::Error::Success;

    return ret;
    }   



//////////////////////////////////////////////////////////////////////////
ProxyManager::ProxyManager() : 
    m_ConnectionString("Server=localhost;Database=proxerity;Uid=root;Pwd=TheGwydd;"),
    m_SociBackEnd(*factory_mysql())
	{
	m_LoadingComplete = false;
	}

//////////////////////////////////////////////////////////////////////////
ProxyManager::~ProxyManager()
	{
	}

//////////////////////////////////////////////////////////////////////////
ProxyManager::Error ProxyManager::register_proxy_list_parser(ProxyListParser::Ptr parser)
    {
    ProxyManager::Error ret(ProxyManager::Error::AlreadyRegistered);

    TRACE_NFO("Registering proxy list parser : %s", parser->name().c_str());

    std::lock_guard<std::mutex> lock(m_Mutex);

    if (get_proxy_list_parser(parser->name()) == nullptr)
        {
        m_ProxyListFetchers.push_back(std::make_shared<ProxyListFetcher>(parser));
        ret = ProxyManager::Error::Success;
        }
    else
        TRACE_ERR("A proxy list parser with name %s already exists!", parser->name().c_str());

    return ret;
    }

//////////////////////////////////////////////////////////////////////////
ProxyManager::Error ProxyManager::register_proxy_list_parser(const std::string & name)
    {
    ProxyManager::Error ret(ProxyManager::Error::NotFound);

    TRACE_NFO("Unregistering proxy list parser : %s", name.c_str());

    std::lock_guard<std::mutex> lock(m_Mutex);

    ProxyListFetcher::Ptr vv = get_proxy_list_parser(name);
    if (vv)
        {
        m_ProxyListFetchers.remove(vv);
        ret = ProxyManager::Error::Success;
        }
    else
        TRACE_ERR("No proxy list parser with name %s has been found!", name.c_str());

    return ret;
    }

//////////////////////////////////////////////////////////////////////////
ProxyListFetcher::Ptr ProxyManager::get_proxy_list_parser(const std::string & name)
    {
    ProxyListFetcher::Ptr ret(nullptr);

    auto it = std::find_if(m_ProxyListFetchers.begin(), m_ProxyListFetchers.end(), [&](ProxyListFetcher::Ptr parser) { return (parser->name() == name); });
    if (it != m_ProxyListFetchers.end())
        ret = *it;

    return ret;
    }


//////////////////////////////////////////////////////////////////////////
ProxyManager::Error ProxyManager::start()
	{
	Error ret(Error::Success);

	TRACE_NFO("Starting Proxy manager...");

    ret = load_from_db();
    if (ret == Error::Success)
        {
        std::lock_guard<std::mutex> lock(m_Mutex);

        // start all registered fetchers
        for (auto itm : m_ProxyListFetchers)
            itm->start();
        }

	return ret;
	}

//////////////////////////////////////////////////////////////////////////
ProxyManager::Error ProxyManager::stop()
	{
	Error ret(Error::GenericFailure);

	TRACE_NFO("Stopping Proxy manager...");

    std::lock_guard<std::mutex> lock(m_Mutex);

    for (auto itm : m_ProxyListFetchers)
        itm->stop();

	return ret;
	}

//////////////////////////////////////////////////////////////////////////
ProxyManager::Error ProxyManager::add_proxy(Proxy::Ptr proxy)
    {
    ProxyManager::Error ret(ProxyManager::Error::AlreadyRegistered);

    TRACE_NFO("Adding proxy : %s", proxy->to_string().c_str());

    std::lock_guard<std::mutex> lock(m_Mutex);

    auto it = std::find_if(m_Proxies.begin(), m_Proxies.end(), [=](Proxy::Ptr p) { 
        return ((p->address() == proxy->address()) && (p->port() == proxy->port()));
        });

    if (it != m_Proxies.end())
        {
        TRACE_ERR("Proxy already existing. Updating data!");
        (*it)->supports_ssl(proxy->supports_ssl());
        }
    else
        {
        m_Proxies.push_back(proxy);
        ret = ProxyManager::Error::Success;
        }

    return ret;
    }

//////////////////////////////////////////////////////////////////////////
Proxy::Ptr ProxyManager::get_proxy(const std::string & domain)
	{
	Proxy::Ptr ret(nullptr);

	std::lock_guard<std::mutex> lock(m_Mutex);

    system_clock::time_point current_time = system_clock::now();

	// the best would be a proxy we have never used
	ret = get_unused_proxy(domain);
	if (!ret)
		// now, the best is the one with the oldest access to requested domain 
		ret = get_oldest_used_proxy(domain, current_time);

	// holy shit! I can't find a proxy...
	if (!ret)
		TRACE_DBG("Cannot find a proxy to use with domain : %s", domain.c_str());
	else
        {
        // mark the proxy as used for the requested domain
        ret->add_domain(domain);
        ret->increment_use_count();
        }

	return ret;
	}


//////////////////////////////////////////////////////////////////////////
Proxy::Ptr ProxyManager::get_unused_proxy(const std::string & domain_name)
	{
	Proxy::Ptr ret(nullptr);

	auto it = std::find_if(m_Proxies.begin(), m_Proxies.end(), [&](Proxy::Ptr proxy) {
		return (!proxy->is_bad_proxy() && !proxy->has_domain(domain_name)); 
		});

	if (it != m_Proxies.end())
		ret = *it;

	return ret;
	}

//////////////////////////////////////////////////////////////////////////
Proxy::Ptr ProxyManager::get_oldest_used_proxy(const std::string & domain_name, system_clock::time_point & current_time)
	{
	Proxy::Ptr ret(nullptr);
	long long best_value = 0;

	for (Proxy::Ptr p : m_Proxies)
		{
		if (!p->is_bad_proxy())
			{
			long long p_value = p->get_domain_access_timespan_ms(domain_name, current_time);
			if (p_value > best_value)
				{
				ret = p;
				best_value = p_value;
				}
			}
		}

	return ret;
	}

/*
#include <mysqld_error.h>
#include "mysql/test-mysql.h"
*/

//////////////////////////////////////////////////////////////////////////
ProxyManager::Error ProxyManager::load_from_db()
    {
    ProxyManager::Error ret(ProxyManager::Error::DbError);

    TRACE_NFO("Loading proxies from DB");

    std::lock_guard<std::mutex> lock(m_Mutex);

    session sql(m_SociBackEnd, m_ConnectionString);
    mysql_session_backend * sessionBackEnd = static_cast<mysql_session_backend *>(sql.get_backend());
/*
    std::string version = mysql_get_server_info(sessionBackEnd->conn_);
    std::istringstream iss(version);
    if ((iss >> v) and v < 5)
        {
        WARN("MySQL server version " << v
             << " does not support stored procedures, skipping test.");
        return;
        }

    try { sql << "drop function myecho"; }
    catch (soci_error const &) {}

    std::string in("my message");
    std::string out;
    statement st = (sql.prepare << "select * from proxy_table", into(out), use(in, "input"));

    st.execute(1);
*/

    return ret;
    }
