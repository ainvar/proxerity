#include "TimerThread.h"

//////////////////////////////////////////////////////////////////////////
TimerThread::TimerThread(milliseconds time, TimerCallback callback, bool one_shot, void * argument) :
	m_Run(true),
	m_Running(false),
	m_OneShot(one_shot),
	m_Period(time),
	m_Argument(argument),
	m_Callback(callback)
	{
	}

//////////////////////////////////////////////////////////////////////////
TimerThread::~TimerThread()
	{
	stop();
	}

//////////////////////////////////////////////////////////////////////////
bool TimerThread::start()
	{
	if (this->running() == true)
		return false;

	m_Running = true;
	m_Run = true;
	reset();

	m_Thread = std::thread([&]() {
		while (m_Run) {
			if (elapsed() >= m_Period) {
				m_Callback(m_Argument);
				reset();
				if (m_OneShot)
					m_Run = false;
				}
			else
				std::this_thread::yield();
			}

		m_Running = false;
		});

	return true;
	}

//////////////////////////////////////////////////////////////////////////
bool TimerThread::stop()
	{
	if (m_Running)
		{
		m_Run = false;
		m_Thread.join();
		}

	return true;
	}
