#include "../include/Version.h"
#include "../include/Logger.h"
#include "../include/./TimerThread.h"
#include "../include/ConfigurationManager.h"
#include "../include/ProxyManager.h"
#include "../include/ProxySpyParser.h"
#include "../include/ProxerityController.h"

#include <filesystem>
using namespace std::experimental;

//////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
	{
	TRACE_DBG("proxerity v%d.%d.%d", PROXERITY_VERSION_MAJOR, PROXERITY_VERSION_MINOR, PROXERITY_VERSION_BUILD);

    filesystem::path exe_path = filesystem::system_complete(argv[0]);
    filesystem::path exe_folder = exe_path.remove_filename();

    bool err = false;

    // load the configuration
    if (ConfigurationManager::Instance().load((exe_folder / "config.json").string()) != ConfigurationManager::Error::Success)
        err = true;

    if (!err)
        err = (ProxyManager::Instance().register_proxy_list_parser(std::make_shared<ProxySpyParser>()) != ProxyManager::Error::Success);

    if (!err)
        err = (ProxyManager::Instance().start() != ProxyManager::Error::Success);

    if (!err)
        {
        try
            {
            ProxerityController controller("http://localhost/proxerity");
            controller.start();

            do {} while (cin.get() != 'x');
            }
        catch (exception const & e)
            {
            cout << e.what() << endl;
            }
        }

    return 0;
	}

