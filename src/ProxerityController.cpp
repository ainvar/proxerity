#include "../include/ProxerityController.h"
#include "../include/ProxyManager.h"
#include "../include/Logger.h"


//////////////////////////////////////////////////////////////////////////
ProxerityController::ProxerityController(const std::string & url) : RestController(url, SUPPORT_GET | SUPPORT_POST)
    {
    }

//////////////////////////////////////////////////////////////////////////
ProxerityController::~ProxerityController()
    {
    }

//////////////////////////////////////////////////////////////////////////
void ProxerityController::on_get(http_request message)
    {
    ProxyGetArguments args;
    if (args.from_json(message.extract_json().get()) == SerializerError::Success)
        {
        Proxy::Ptr proxy = ProxyManager::Instance().get_proxy(args.m_Domain);
        if (proxy)
            message.reply(http::status_codes::OK, *proxy);
        else
            message.reply(http::status_codes::InternalError);
        }
    else
        message.reply(http::status_codes::BadRequest);
    }

//////////////////////////////////////////////////////////////////////////
void ProxerityController::on_post(http_request message)
    {
    ProxyGetArguments args;
    if (args.from_json(message.extract_json().get()) == SerializerError::Success)
        {
        Proxy::Ptr proxy = ProxyManager::Instance().get_proxy(args.m_Domain);
        if (proxy)
            message.reply(http::status_codes::OK, *proxy);
        else
            message.reply(http::status_codes::InternalError);
        }
    else
        message.reply(http::status_codes::BadRequest);
    }


